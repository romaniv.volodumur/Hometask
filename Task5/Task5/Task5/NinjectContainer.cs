﻿using Ninject.Modules;
using Task5.Interfaces;

namespace Task5
{
    class NinjectContainer : NinjectModule
    {
        public override void Load()
        {
            Bind<ISerializer>().To<CSVSerializer>();
        }
    }
}
