﻿using System.Collections.Generic;
using Task5.Model;

namespace Task5.Interfaces
{
    public interface ISerializer
    {
        void Serialize<T>(T value, string path);
        List<User> Deserialize(string path);
    }
}
