﻿using Task5.Model;

namespace Task5.Interfaces
{
    public interface IView
    {
        void Close();
        User InitializeModel();
    }
}
