﻿using Ninject;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Task5.Interfaces;
using Task5.Model;

namespace Task5
{
    public partial class SaveUserForm : Form, IView
    {
        private Presenter presenter = null;
        private IKernel ninjectKernel = null;
        public SaveUserForm()
        {
            ninjectKernel = new StandardKernel(new NinjectContainer());
            presenter = new Presenter(this);
            presenter.UserAdded += ChangeHistory;
            InitializeComponent();
        }
        public User InitializeModel()
        {
            return new User()
            {
                FirstName = FirstName.Text,
                LastName = LastName.Text,
                Age = Convert.ToInt32(Age.Value),
                PlaceOfWork = PlaceOfWork.Text
            };
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            presenter.CloseView();
        }
        private void AddUserButton_Click(object sender, EventArgs e)
        {
            presenter.SerializeToCSV(ninjectKernel.Get<ISerializer>(), Path.Text);
        }
        private string GetPath()
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var fileInfo = new FileInfo(fileDialog.FileName);
                return fileInfo.FullName;
            }
            return string.Empty;
        }
        private void BrowseButton_Click(object sender, EventArgs e)
        {
            Path.Text = GetPath();
        }
        private void ChangeHistory(object sender, User user)
        {
            History.Text += $"{DateTime.Now.ToShortTimeString()} added user '{user.FirstName} {user.LastName}'" + Environment.NewLine;
        }
        private void CreateFileButton_Click(object sender, EventArgs e)
        {
            var fileDialog = new FolderBrowserDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                Path.Text = fileDialog.SelectedPath + "\\" + (new Random()).Next(1, 1000) + ".csv";
            }
        }
        private void ShowUsersButton_Click(object sender, EventArgs e)
        {
            Path.Text = GetPath();
            UsersHistory.Clear();
            var users = presenter.DeserializeFromCSV(ninjectKernel.Get<ISerializer>(), Path.Text);
            int usersToTake = 10;
            if (users.Count != 0)
            {
                if (usersToTake < users.Count)
                {
                    var lastUsers = users.Skip(users.Count - usersToTake);
                    foreach (var user in lastUsers)
                    {
                        UsersHistory.Text += user + Environment.NewLine;
                    }
                }
                else
                {
                    var lastUsers = users.Skip(users.Count / 2);
                    foreach (var user in lastUsers)
                    {
                        UsersHistory.Text += user + Environment.NewLine;
                    }
                }
            }
            else
            {
                MessageBox.Show("There is no users in file!");
            }
        }
        private void SearchButton_Click(object sender, EventArgs e)
        {
            Path.Text = GetPath();
            UsersHistory.Clear();
            var users = presenter.DeserializeFromCSV(ninjectKernel.Get<ISerializer>(), Path.Text);
            if (!string.IsNullOrEmpty(SearchedFirstName.Text) && !string.IsNullOrEmpty(SearchedLastName.Text))
            {
                var targetUsers = users.Where(x => x.FirstName == SearchedFirstName.Text && x.LastName == SearchedLastName.Text);
                foreach (var item in targetUsers)
                {
                    SearchedUsers.Text += item + Environment.NewLine;
                }
            }
            else
            {
                SearchedUsers.Text = "No users found";
            }

        }
    }
}
