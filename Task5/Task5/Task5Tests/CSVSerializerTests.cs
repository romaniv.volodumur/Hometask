﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Task5;
using NUnit;
using NUnit.Framework;
using System;
using Task5.Model;
using System.Collections.Generic;

namespace Task5Tests
{
    [TestFixture]
    public class CSVSerializerTests
    {
        [Test]
        public void Serialize_ValueIsNullAndStringIsEmpty_ThrowsArgumentNullException()
        {
            CSVSerializer serializer = new CSVSerializer();
            Assert.Throws<ArgumentNullException>(() => serializer.Serialize<User>(null, ""));
        }
        [Test]
        public void Deserialize_PathAsStringInput_ReturnsList()
        {
            CSVSerializer serializer = new CSVSerializer();
            var result = serializer.Deserialize(@"C:\Users\ПК\Desktop\Eleks Hometask\2.csv");

            Assert.That(result, Is.TypeOf<List<User>>());
        }
        [Test]
        public void Deserialize_EmptyStringInput_ThrowsArgumentException()
        {
            CSVSerializer serializer = new CSVSerializer();
            Assert.Throws<ArgumentException>(() => serializer.Deserialize(""));
        }
    }
}
