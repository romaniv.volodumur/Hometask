﻿using System;

namespace LibraryProject
{
    public abstract class Department : IComparable, ICountingBooks
    {
        public Book[] books;
        public abstract string NameOfSection { get; }
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            Department comparer = obj as Department;
            if (comparer != null)
            {
                return books.Length.CompareTo(comparer.books.Length);
            }
            return 1;
        }
        public int GetNumberOfBooks()
        {
            return books.Length;
        }
        public override string ToString()
        {
            return $"Section name:{ NameOfSection }";
        }
    }
}
