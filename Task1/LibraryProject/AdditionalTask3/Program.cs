﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdditionalTask3
{
    class Program
    {
        static int RowSum(int[] row)
        {
            int sum = 0;
            for (int i = 0; i < row.Length; i++)
            {
                sum += row[i];
            }
            return sum;
        }
        static int RowMinElementPosition(int[] row)
        {
            int min = row[0];
            int position = 0;
            for (int i = 0; i < row.Length; i++)
            {
                if (row[i] < min)
                {
                    min = row[i];
                    position = i;
                }
            }
            return position;
        }
        static Dictionary<int, int> FindSaddlePoints(int[][] matrix)
        {
            var coordinates = new Dictionary<int, int>();
            bool isLower = false;
            for (int i = 0; i < matrix.Length; i++)
            {
                isLower = false;
                int positionOfMinElement = RowMinElementPosition(matrix[i]);
                for (int j = 0; j < matrix.Length - i; j++)
                {
                    if (matrix[j][positionOfMinElement] > matrix[i][positionOfMinElement])
                    {
                        isLower = true;
                        break;
                    }
                }
                if (!isLower)
                {
                    coordinates.Add(i, positionOfMinElement);
                }
            }
            return coordinates;
        }
        static Dictionary<int, int> FindRowsSum(int[][] matrix)
        {
            var sumValues = new Dictionary<int, int>();
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] < 0)
                    {
                        sumValues.Add(i, RowSum(matrix[i]));
                        break;
                    }
                }
            }
            return sumValues;
        }
        static void Main(string[] args)
        {
            int[][] matrix = new int[3][]
            {
                new int[]{ -2, 3, -3 },
                new int[]{ 4, 8, 6 },
                new int[] { -3, -8, -5 }
            };

            var results = FindSaddlePoints(matrix);
            Console.WriteLine("Saddle points: ");
            foreach (var item in results)
            {
                Console.WriteLine($"({item.Key}, {item.Value})");
            }
            Console.WriteLine("Sum of rows with negative elements:");
            results = FindRowsSum(matrix);
            foreach (var item in results)
            {
                Console.WriteLine($"Row {item.Key} : {item.Value}");
            }
            Console.ReadKey();
        }
    }
}
