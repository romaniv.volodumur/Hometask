﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewLibrary
{
    class Program
    {
        static Author GetMostPublishedAuthor(params Department[] sections)
        {
            Author maxPublishes = sections[0].books[0].Author;
            for (int i = 0; i < sections.Length; i++)
            {
                for (int j = 0; j < sections[i].books.Count; j++)
                {
                    if (sections[i].books[j].Author.CompareTo(maxPublishes) == 1)
                    {
                        maxPublishes = sections[i].books[j].Author;
                    }
                }
            }
            return maxPublishes;
        }
        static Department GetBiggestSection(params Department[] sections)
        {
            int maxNumber = sections[0].GetNumberOfBooks();
            int index = 0;
            for (int i = 0; i < sections.Length; i++)
            {
                if (sections[i].GetNumberOfBooks() > maxNumber)
                {
                    maxNumber = sections[i].GetNumberOfBooks();
                    index = i;
                }
            }
            return sections[index];
        }
        static Book GetSmallestBook(params Department[] sections)
        {
            Book smallestBook = sections[0].books[0];
            for (int i = 0; i < sections.Length; i++)
            {
                for (int j = 0; j < sections[i].books.Count; j++)
                {
                    if (sections[i].books[j].CompareTo(smallestBook) == -1)
                    {
                        smallestBook = sections[i].books[j];
                    }
                }
            }
            return smallestBook;
        }

        static void Main(string[] args)
        {
            XMLHelper.DeleteBook("Library.xml", "Javascript ninja");
            Console.ReadKey();
        }
    }
}
